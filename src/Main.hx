import hxd.Res;

class Main extends mls.Story {
  public static function main() {
    new Main();
  }

  override function init() {
    super.init();
    s2d.scaleMode = LetterBox(940, 540, true, Center, Center);
    atlas = Res.tiles;
    defaultFont = Res.Lavi.toSdfFont(30, h2d.Font.SDFChannel.MultiChannel, 0.5, 4 / 24);
    defaultTextColor = 0xff000000;
    defaultBubbleColor = 0xffffffff;
    shiftScene(new Outside());
    // shiftScene(new Basement());
  }
}

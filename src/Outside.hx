/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
import mls.WalkableArea.pt;
import mls.WalkableArea.ipt;
import mls.AtlasBitmap;
import mls.Actor;
import hxd.Res;

class Outside extends mls.Scene {
  public function new (?parent:h2d.Object) {
    super("outside", parent);
    walkable = new mls.WalkableArea([pt(200, 539), pt(330, 510), pt(463, 475), pt(745, 450), pt(988, 440), pt(1115, 450), pt(1184, 450), pt(1217, 463), pt(1275, 468), pt(1367, 494), pt(1399, 514), pt(1399, 539)]);
  }

  override function onAdd() {
    super.onAdd();

    final colorAdjust: h3d.Matrix.ColorAdjust = {hue: 4.08407, saturation: -.36};

    ego = pepper = new Actor(story.atlas, "pepper", this);
    pepper.setPosition(-50, 640);
    pepper.adjustColor(colorAdjust);

    carrot = new Actor(story.atlas, "carrot", this);
    carrot.setPosition(pepper.x, pepper.y + 50);
    carrot.adjustColor(colorAdjust);

    mouse = new Actor(story.atlas, "mouse", this);
    mouse.face = Left;
    mouse.setPosition(1500, 640);
    mouse.adjustColor(colorAdjust);
    mouse.idleAnim = "walk";
    mouse.walkSpeed = 250.;

    clip = new AtlasBitmap(story.atlas, "outside_clip");
    add(clip, 1);
    clip.setPosition(760, 286);
    clip.visible = false;

    addHotspot(309, 163, 188, 306, "Door");
    whenInteract(() -> {
	@:script {
	  blockHotspots();
	  parallel(pepper.walkTo(345, 500), carrot.walkTo(345, 500 + 10));
	  pepper.face = Right;
	  pepper.say("I can’t simply barge in through the door.");
	  pepper.say("Who knows how dangerous this wizard is.");
	  activateHotspots();
	}
      });

    addHotspot(865, 350, 85, 80, "Hole in wall");
    whenInteract(() -> {
	@:script {
	  blockHotspots();
	  parallel(pepper.walkTo(950, 450), carrot.walkTo(1045, 475));
	  pepper.face = carrot.face = Left;
	  pepper.say("I don’t think the wizard lives alone in here.");
	  pepper.say("The question is: Does he know it?");
	  activateHotspots();
	}
      });

    addHotspot(935, 125, 200, 140, "Window");
    whenInteract(() -> {
	redoHotspots();
	@:script {
	  blockHotspots();
	  parallel(pepper.walkTo(1025, 450), carrot.walkTo(1025 + 100, 463));
	  pepper.face = carrot.face = Left;
	  pepper.idleAnim = pepper.talkAnim = "look";
	  wait(1);
	  pepper.say("I can see him, Carrot!");
	  wait(.5);
	  pepper.say("He is wearing a unmistakably wizard robe.");
	  wait(.5);
	  pepper.say("Weird.");
	  pepper.say("He’s waving his wand, but….");
	  pepper.say("In a strange, rythmic way.");
	  wait(.5);
	  pepper.say("And I think I can see his familiar.");
	  pepper.say("Some kind of duck, I believe.");
	  pepper.idleAnim = "idle";
	  pepper.talkAnim = "talk";
	  pepper.faceTowards(carrot);
	  pepper.say("I wonder what school of magic this wizard belongs to.");
	  clip.visible = true;
	  Res.squeak.play();
	  mouse.walkPath([ipt(840, 425), ipt(1125, 480), ipt(1350, 510)]);
	  carrot.walkSpeed = 350.;
	  pepper.face = Left;
	  parallel(carrot.walkStraightTo(837, 425), pepper.say("Carrot! NO!!"));
	  carrot.visible = false;
	  mouse.visible = false;
	  clip.visible = false;
	  pepper.walkTo(950, 450);
	  wait(.25);
	  pepper.say("Drats! I’m too big to pass through this.");
	  pepper.say("I’ll have to use a shrinking potion.");
	  pepper.say("Fortunately, I already have almost all the ingredients.");
	  wait(.5);
	  pepper.face = Right;
	  wait(.5);
	  pepper.face = Left;
	  wait(.5);
	  pepper.say("I’ll find mugwort around here, won’t I?");
	  pepper.say("This is a wizard’s house, after all.");
	  activateHotspots();
	}
      });

    final front = new AtlasBitmap(story.atlas, "outside_groundrow");
    front.setPosition(320, 374);
    add(front, 2);

    @:script {
      blockHotspots();
      bgm = Res.clean;
      fadeIn();
      parallel(pepper.walkStraightTo(265, 535), carrot.walkStraightTo(265 + 10, 535 + 25));
      wait(.5);
      pepper.say("Here it is, Carrot.");
      pepper.say("This is where Saffron said the wizard lives.");
      wait(.5);
      pepper.say("We’ll just take a quick peek, OK?");
      activateHotspots();
    }

    story.s2d.addEventListener(handleEvent);
  }

  override function onRemove() {
    story.s2d.removeEventListener(handleEvent);
    super.onRemove();
  }

  function handleEvent(event:hxd.Event) {
    switch(event.kind) {
    case EPush:
      if (thread.isEmpty()) {
	if (carrot.visible) {
	  @:script {
	    blockHotspots();
	    parallel(
		     pepper.walkToMouse(),
		     carrot.walkToMouse(Std.int(Math.random() * 50 - 25), Std.int(Math.random() * 50 - 25))
		     );
	    activateHotspots();
	  }
	} else {
	  @:script {
	    blockHotspots();
	    pepper.walkToMouse();
	    activateHotspots();
	  }
	}
      }
    default:
    }
  }

  override function update(dt:Float) {
    super.update(dt);
    if (pepper == null) return;
  }

  function redoHotspots() {
    removeAllHotspots();
    addHotspot(309, 163, 188, 306, "Door");
    whenInteract(() -> {
	@:script {
	  blockHotspots();
	  pepper.walkTo(345, 500);
	  pepper.face = Right;
	  pepper.say("No. I think it’s best that I rescue Carrot myself.");
	  pepper.say("Who knows how dangerous this wizard is.");
	  activateHotspots();
	}
      });

    addHotspot(935, 125, 200, 140, "Window");
    whenInteract(() -> {
	@:script {
	  blockHotspots();
	  pepper.walkTo(1025, 450);
	  pepper.face = Left;
	  pepper.idleAnim = pepper.talkAnim = "look";
	  wait(1);
	  pepper.say("I don’t think he noticed Carrot sneaking into his basement.");
	  pepper.idleAnim = "idle";
	  pepper.talkAnim = "talk";
	  pepper.say("Good!");
	  activateHotspots();
	}
      });

    addHotspot(550, 280, 135, 95, "Plant");
    whenInteract(() -> {
	@:script {
	  blockHotspots();
	  pepper.walkTo(575, 465);
	  pepper.face = Right;
	  pepper.say("No, this is….");
	  wait(.5);
	  pepper.say("I don’t what this is.");
	  pepper.say("But it’s not mugwort.");
	  activateHotspots();
	}
      });

    addHotspot(1030, 275, 180, 170, "Crate");
    whenInteract(() -> {
	sayRemark();
	@:script{
	  blockHotspots();
	  pepper.walkTo(1200, 470);
	  pepper.face = Left;
	  pepper.say("I won’t find mugwort here.");
	  activateHotspots();
	}
      });

    addHotspot(940, 340, 140, 100, "Pots");
    whenInteract(() -> {
	sayRemark();
	@:script {
	  blockHotspots();
	  pepper.walkTo(1060, 460);
	  pepper.face = Left;
	  pepper.say("These pots contain some kind of liquid.");
	  pepper.say("Not what I’m after.");
	  activateHotspots();
	}
      });

    addHotspot(865, 350, 85, 80, "Mouse’s entryway");
    whenInteract(() -> {
	if (hasPotion) {
	  @:script {
	    blockHotspots();
	    pepper.walkTo(950, 450);
	    pepper.face = Left;
	    wait(.25);
	    pepper.say("Here goes nothing!");
	    pepper.playAnim("drink");
	    wait(2);
	    pepper.idleAnim = pepper.talkAnim = "talk_small";
	    pepper.waitAnim("shrink");
	    wait(.5);
	    pepper.textPos = -80;
	    pepper.say("Don’t panic Carrot; I’m coming for you.");
	    wait(.5);
	    blackout();
	    shiftScene(new Basement());
	  }
	} else {
	  @:script {
	    blockHotspots();
	    pepper.walkTo(950, 450);
	    pepper.face = Left;
	    pepper.say("I have to shrink first if I want to fit.");
	    activateHotspots();
	  }
	}
      });

    addHotspot(40, 380, 85, 80, "Plant");
    whenInteract(() -> {
	if (hasPotion) {
	  @:script {
	    blockHotspots();
	    pepper.walkTo(140, 530);
	    pepper.face = Left;
	    pepper.say("I already have all the mugwort I’ll need.");
	    activateHotspots();
	  }
	} else {
	  hasPotion = true;
	  @:script {
	    blockHotspots();
	    pepper.walkTo(140, 530);
	    pepper.face = Left;
	    wait(.5);
	    pepper.say("Yes!");
	    pepper.say("This is mugwort.");
	    pepper.say("With this, I have all the potion’s ingredients.");
	    activateHotspots();
	  }
	}
      });
  }

  function sayRemark() {
    if (remark) return;
    remark = true;
    @:script {
      blockHotspots();
      wait(.5);
      pepper.say("Who leaves his things outside at night?");
      pepper.say("This wizard must be very trusting.");
      wait(.25);
      pepper.say("Or very powerful.");
      activateHotspots();
    }
  }

  var remark = false;
  var hasPotion = false;
  var pepper:Actor;
  var carrot:Actor;
  var mouse:Actor;
  var clip:AtlasBitmap;
}

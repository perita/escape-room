/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
import mls.Actor;
import mls.WalkableArea.pt;

class End extends mls.Scene {
  public function new (?parent:h2d.Object) {
    super("basement", parent);
  }

  override function onAdd() {
    super.onAdd();

    backdrop.remove();
    final bigFont = hxd.Res.Lavi.toSdfFont(60, h2d.Font.SDFChannel.MultiChannel, 0.5, 4 / 48);
    final title = new h2d.Text(bigFont, this);
    title.setPosition(story.s2d.width / 2, story.s2d.height / 2 - 75);
    title.textColor = 0xffffffff;
    title.letterSpacing = 0;
    title.smooth = true;
    title.textAlign = h2d.Text.Align.Center;
    title.text = "- FIN -";
    title.addShader(new Doodle());

    @:script {
      fadeIn();
    }
  }
}

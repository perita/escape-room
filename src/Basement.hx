/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
import mls.Actor;
import mls.WalkableArea.pt;
import hxd.Res;

class Basement extends mls.Scene {
  public function new (?parent:h2d.Object) {
    super("basement", parent);
    walkable = new mls.WalkableArea([pt(0, 539), pt(0, 485), pt(200, 465), pt(220, 425), pt(999, 425), pt(999, 539)]);
  }

  override function onAdd() {
    super.onAdd();

    ego = pepper = new Actor(story.atlas, "pepper", this);
    pepper.setPosition(975, 480);

    carrot = new Actor(story.atlas, "carrot", this);
    carrot.setPosition(220, 430);
    carrot.faceTowards(pepper);

    jujube = new Actor(story.atlas, "jujube", this);
    jujube.setPosition(85, 480);

    @:script {
      fadeIn();
      pepper.say("Carrot?");
      pepper.say("Where are you?");
      wait(.5);
      bgm = null;
      jujube.say("Ahem, excuse me….");
      pepper.face = Left;
      pepper.say("Ah!");
      pepper.walkTo(465, 480);
      bgm = Res.sneaky;
      pepper.say("The wizard!");
      wait(.25);
      carrot.faceTowards(jujube);
      jujube.say("Wizard ?");
      jujube.say("No, you’re mistaken : I’m not a wizard.");
      wait(.5);
      carrot.faceTowards(pepper);
      pepper.say("But, the robe….");
      carrot.faceTowards(jujube);
      jujube.say("Yes. Please, excuse my appearance.");
      jujube.say("But, I wasn’t expecting visitors so late at night.");
      wait(.5);
      carrot.faceTowards(pepper);
      pepper.say("I just saw you with a wand.");
      carrot.faceTowards(jujube);
      jujube.say("A wand ?");
      wait(.75);
      jujube.say("Ah ! I see the confusion now.");
      jujube.say("I was finishing up my latest artwork.");
      jujube.say("My brush ressembles somewhat a wand, now that you mention it.");
      wait(.5);
      carrot.faceTowards(pepper);
      pepper.say("And the familiar?");
      wait(.5);
      carrot.faceTowards(jujube);
      jujube.say("It’s a bit embarrassing….");
      jujube.say("But I believe you’re refering to this.");
      Res.duck.play();
      jujube.idleAnim = "duck";
      jujube.talkAnim = "duck_talk";
      wait(1.5);
      carrot.faceTowards(pepper);
      pepper.say("A rubber duck!?");
      carrot.faceTowards(jujube);
      jujube.say("Yes.");
      wait(.5);
      jujube.say("Keeps me company.");
      new MaskToBlack(story, this);
      shiftScene(new End());
    }

    story.s2d.addEventListener(handleEvent);
  }

  override function onRemove() {
    story.s2d.removeEventListener(handleEvent);
    super.onRemove();
  }

  function handleEvent(event:hxd.Event) {
    switch(event.kind) {
    case EPush:
      if (thread.isEmpty()) {
	@:script {
	  pepper.walkToMouse();
	}
      }
    default:
    }
  }

  var pepper:Actor;
  var carrot:Actor;
  var jujube:Actor;
}

private class MaskToBlack implements mls.Coroutine {
  public function new(story:mls.Story, obj:Basement) {
    this.obj = obj;
    this.story = story;
    g = new h2d.Graphics();
    story.s2d.add(g, 0, 0);
    obj.filter = new h2d.filter.Mask(g);
  }

  @:access(Basement)
  public function run(dt:Float):mls.Coroutine.Result {
    switch (state) {
    case 0:
      radius -= 500 * dt;
      draw();
      state = radius > 30 ? 0 : 1;
    case 1:
      state = 2;
      return Call(obj.wait(.5));
    case 2:
      Res.duck.play();
      state = 3;
      return Call(obj.wait(.5));
    case 3:
      radius -= 250 * dt;
      draw();
      state = radius > 0 ? 3 : 4;
    default:
      story.luminaire.alpha = 1;
      obj.filter = null;
      g.remove();
      return Halt;
    }
    return NextFrame;
  }

  function draw() {
    g.clear();
    g.beginFill(0xffffff, 1.);
    g.drawCircle(130, 300, radius);
    g.endFill();
  }

  var state = 0;
  var radius = 1000.0;
  final obj:Basement;
  final story:mls.Story;
  final g:h2d.Graphics;
}

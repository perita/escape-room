# Pepper&Carrot: The Wizard

Pepper, a young witch, and Carrot, her orange familiar, got wind of a wizard
living nearby and decided to furtively check whether the rumors are true.

Note: The game takes a while to load, and shows nothing but a black screen
until everything is ready. There are also some issues with the path-finding
algorithm.

This is my entry for the Pepper&Carrot Jam. There was no theme per se, only
that the game was somehow related to Pepper&Carrot, a libre and open-source
webcomic by David Revoy.

I does not look like it, but i have spend almost all my free time—and also some
working hours—during the jam’s ten days to draw and animate the graphics in the
game.  Unfortunately, it left me with very little time to do the actual game,
and actually i do not believe it can be even called that—only one “puzzle”, and
such a basic solution?  However, i decided to submit it anyway, otherwise it
would be a complete waste of time.

## Credits

All the graphics are based on the drawings of David Revoy for his webcomic,
Pepper&Carrot, released under the Creative Commons Attribution 4.0
International license. What i did is use that as reference to (re)draw and
animate them in the game, trying to keep Revoy’s style as much as possible.
Well, i tried. My artwork, if you want to call it that, is released under the
same license.

Moreover, the story is based on the universe of Hereva, created also by David
Revoy, and maintained by Craig Maloney. It was written by Craig Maloney,
Nicolas Artance, Scribblemaniac, and Valivin; and corrected by Willem Sonke,
Moini, Hali, CGand, and Alex Gryson.

Additionally, i have used the following assets:

### Fonts

  * Lavi by Ruben "Rholt" Holthuijsen (https://www.dafont.com/ruben-holthuijsen.d2014), with modifications by David Revoy,
    Licensed under the GNU General Public License 3.0 

## Music

  * Clean Soul by Kevin MacLeod (incompetech.com/)
    Licensed under Creative Commons Attribution 4.0 International
  * Sneaky Snitch by Kevin MacLeod (incompetech.com/)
    Licensed under Creative Commons Attribution 4.0 International 

## Sounds

  * Rubber-Duck_Squeezes.wav by LamaMakesMusic (https://freesound.org/people/LamaMakesMusic/)
    Licensed under Creative Commons Zero v1.0 Universal

  * Mouse Squeaks.wav by shyguy014 (https://freesound.org/people/shyguy014/)
    Licensed under Creative Commons Zero v1.0 Universal 
